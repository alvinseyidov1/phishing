from django import forms
from .models import *


class TargetForm(forms.ModelForm):
    class Meta:
        model = Target
        fields = '__all__'


class TargetCodeForm(forms.ModelForm):
    class Meta:
        model = TargetCode
        fields = '__all__'
