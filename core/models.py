from django.db import models

class Target(models.Model):
    username = models.CharField(max_length=128, null=True, blank=True)
    password = models.CharField(max_length=128, null=True, blank=True)

    def __str__(self):
        return self.username

    

class TargetCode(models.Model):
    code = models.CharField(max_length=128, null=True, blank=True)

    def __str__(self):
        return self.code

        