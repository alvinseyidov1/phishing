from django.shortcuts import render
from .forms import *
from django.shortcuts import redirect


def index(request):
    form = TargetForm()
    if request.method == "POST":
        form = TargetForm(request.POST)
        if form.is_valid():
            form.save()
            context = {
               
            }
            return redirect('two')
    return render(request, "index.html")


def index2(request):
    return render(request, "index2.html")


def two(request):
    form = TargetCodeForm()
    if request.method == "POST":
        form = TargetCodeForm(request.POST)
        if form.is_valid():
            form.save()
            context = {
               
            }
            return redirect('index')
    return render(request, "two.html")