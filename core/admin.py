from django.contrib import admin
from .models import *


admin.site.register(Target)
admin.site.register(TargetCode)